<?php

class Elang {
    use Hewan, Fight;
    
    public function __construct ($nama='', $jumlahKaki='2', $keahlian= 'terbang tinggi', $attackPower =10, $defencePower=5) {
        $this->nama = $nama;
        $this->jumlahKaki = $jumlahKaki;
        $this->keahlian = $keahlian;
        $this->attackPower = $attackPower;
        $this->defencePower = $defencePower;
    }

    public function getInfoHewan () {
        $str = "Jenis hewan   : ELANG " . "<br>" .
               "Nama          : " . $this->nama . "<br>" .
               "Darah         : " . $this->darah . "<br>" .               
               "Jumlah Kaki   : " . $this->jumlahKaki . "<br>" .
               "Keahlian      : " . $this->keahlian . "<br>" .
               "Attack Power  : " . $this->attackPower . "<br>" .
               "Defence Power : " . $this->defencePower . "<br>" ;
        return $str;

    }
}
?>