<?php

class Harimau {
    use Hewan, Fight;
    
    public function __construct ($nama='', $jumlahKaki='4', $keahlian= 'lari cepat', $attackPower =7, $defencePower=8) {
        $this->nama = $nama;
        $this->jumlahKaki = $jumlahKaki;
        $this->keahlian = $keahlian;
        $this->attackPower = $attackPower;
        $this->defencePower = $defencePower;
    }

    public function getInfoHewan () {
        $str = "Jenis hewan   : HARIMAU " . "<br>" .
               "Nama          : " . $this->nama . "<br>" .
               "Darah         : " . $this->darah . "<br>" .               
               "Jumlah Kaki   : " . $this->jumlahKaki . "<br>" .
               "Keahlian      : " . $this->keahlian . "<br>" .
               "Attack Power  : " . $this->attackPower . "<br>" .
               "Defence Power : " . $this->defencePower . "<br>" ;
        return $str;
    }


}
?>