<?php

trait Fight {

    protected $attackPower,
              $defencePower;
    
    public function getattackPower() {
        return $this->attackPower;
    }              
    
    public function serang ($hewan) {
        $str = "{$this->nama} sedang menyerang {$hewan->getnama()}";
        echo $str;
        echo "<br>";
        // setelah hewan1 menyerang hewan2, proses otomatis hewan2 diserang hewan1
        echo $hewan->diserang($this);
        echo "<br>";
        return $str;
    }

    public function diserang ($hewan) {
        //darah sekarang – attackPower penyerang / defencePower yang diserang”
        $berkurang = ($hewan->getattackPower()) / ($this->defencePower);
        $this->darah -= $berkurang;
        $str = "{$this->nama} sedang diserang {$hewan->getnama()}" . "<br>" .
                "Darah {$this->nama} berkurang : {$berkurang}"  . "<br>" .
                "Sisa Darah {$this->nama} : {$this->darah}" ;
        return $str;
    }
}

?>
